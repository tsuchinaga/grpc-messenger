# grpc-messenger

gRPCを使ってチャットもどきを作る

## Interface

* Send: クライアントからサーバへメッセージを送信する
* Receive: クライアントがサーバからメッセージを受信するストリームを開く

## 流れ

1. `go run server/main.go`でサーバを立てます
2. 別のターミナルを開いて、`go run client/main.go`でクライアントを起こします
3. さらに別のターミナルで`go run client/main.go`でクライアントをもう一つ起こします
4. それぞれのクライアントでnameを入力したら、そっからは好きにチャットしまくればいいよ
