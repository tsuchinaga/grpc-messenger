package main

import (
	"context"
	pb "gitlab.com/tsuchinaga/grpc-messenger/proto"
	"google.golang.org/grpc"
	"log"
	"net"
	"sync"
)

const (
	port = ":5432"
)

type server struct {
	num     int
	streams map[int]pb.Messenger_ReceiveServer
	mutex   *sync.Mutex
}

func (s *server) Send(_ context.Context, message *pb.Message) (*pb.SendResult, error) {
	for k, stream := range s.streams {
		err := stream.Send(message)
		if err != nil {
			log.Println(err)
			delete(s.streams, k)
		}
	}

	return &pb.SendResult{Result: true}, nil
}

func (s *server) Receive(_ *pb.ReceiveRequest, stream pb.Messenger_ReceiveServer) error {
	s.mutex.Lock()
	s.streams[s.num] = stream
	s.num++
	s.mutex.Unlock()

	ch := make(chan bool)
	<-ch
	return nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterMessengerServer(s, &server{
		num: 0, streams: make(map[int]pb.Messenger_ReceiveServer), mutex: new(sync.Mutex),
	})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
