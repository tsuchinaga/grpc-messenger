package main

import (
	"context"
	"fmt"
	pb "gitlab.com/tsuchinaga/grpc-messenger/proto"
	"google.golang.org/grpc"
	"log"
)

const (
	address = "localhost:5432"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewMessengerClient(conn)
	ctx := context.Background()

	go func() {
		stream, err := c.Receive(ctx, new(pb.ReceiveRequest))
		if err != nil {
			log.Fatalln(err)
		}

		for {
			if message, err := stream.Recv(); err != nil {
				log.Fatalln(err)
			} else {
				fmt.Printf("%s: %s\n", message.Name, message.Message)
			}
		}
	}()

	var name, message string
	fmt.Print("your name: ")
	_, _ = fmt.Scan(&name)
	for {
		_, _ = fmt.Scan(&message)
		result, err := c.Send(ctx, &pb.Message{Name: name, Message: message})
		if err == nil && result.Result {
			continue
		}
		log.Println(result, err)
	}
}
